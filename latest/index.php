<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href=https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css rel=stylesheet>
<link href=https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/dataTables.bootstrap4.min.css rel=stylesheet>

        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

        <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>

        <script src="https://cdn.datatables.net/1.12.0/js/dataTables.bootstrap5.min.js"></script>

        <title>Mes Stocks</title>
    </head>
    <body>

        <div class="container">
            

            <span id="message"></span>
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col col-sm-9">Mes <b>Stocks</b></div>
                        <div class="col col-sm-3">
                            <button type="button" id="add_data" class="btn btn-success btn-sm float-end"><i class="material-icons">&#xE147;</i> Ajouter un Produit</button>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-sm-8"><h2>Liste des stocks du restaurant</h2></div>
                    <!--
                    <div class="col-sm-4">
                        <div class="search-box">
                            <i class="material-icons">&#xE8B6;</i>
                            <input type="text" id = "search"class="form-control" placeholder="Recherche...">
                        </div>
                    </div>
                    -->

                    
                </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-inverse" id="sample_data" width=100%>
                            <thead>
                                <tr>
                                <th><b>Nom du produit</b></th>
                                <th><b>id</b></th>
                                <th><b>Quantité</b></th>
                                <th><b>Classification</b></th>
                                <th><b>Type de produit</b></th>
                                <th><b>Actions</b></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<div class="modal" tabindex="-1" id="action_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" id="sample_form">
                <div class="modal-header">
                    <h5 class="modal-title" id="dynamic_modal_title"></h5>
                    
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"> x </button>
                </div>
                <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-form-label">Nom du produit <sup style="color:red">*</sup></label>
                            <input type="text" id="productname" class="form-control" name="productname" required="">
                            
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label">Identifiant du produit <sup style="color:red">*</sup></label>
                            <input type="text" id="productid" class="form-control" name="productid" required="">
                        </div>

                    </div>
                    <div class="row">    

                        <div class="col-md-6">
                            <label class="col-form-label">Quantité du produit<sup style="color:red">*</sup></label>
                            <input type="number" id="productqtt" class="form-control" name="productqtt" required="">
                        </div>  
                        
                        <div class="col-md-6">
                            <!--<label class="col-form-label">Comptage<sup style="color:red">*</sup></label>-->

                            <div class="form-group">
                                <label class="form-label">Quantification <sup style="color:red">*</sup></label>
                                    <select name="gender" id="gender" class="form-control">
                                        <option value="Kg">Kg</option>
                                        <option value="L">L</option>
                                        <option value="Unités">Unités</option>
                                        <option value="Cartons">Cartons</option>
                                        <option value="Caisses">Caisses</option>
                                        <option value="Bouteilles">Bouteilles</option>
                                    </select>
                            <!--
                                <select class="selectpicker form-control" id="select_qtt">
                                  <option value="Caisses">Caisses</option>
                                  <option value="Cartons">Cartons</option>
                                  <option value="Unités">Unités</option>
                                  <option value="Kg">Kg</option>
                                  <option value="L">L</option>
                                  <option value="Bouteilles">Bouteilles</option>
                                </select>
                              </div>
                            -->
                            </div>
                        </div>

                    </div>
                    
                    <div class="row"> 
                        <div class="col-md-12">
                            <label class="col-form-label">Type de produit <sup style="color:red">*</sup></label>
                            <input type="text" id="producttype" class="form-control" name="producttype" required="">

                        </div>
                        
                    </div>

                </div>
            </div>
            <!--
                    <div class="mb-3">
                        <label class="form-label">First Name</label>
                        <input type="text" name="first_name" id="first_name" class="form-control" />
                        <span id="productname_error" class="text-danger"></span>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Last Name</label>
                        <input type="text" name="last_name" id="last_name" class="form-control" />
                        <span id="last_name_error" class="text-danger"></span>
                    </div> 
                    <div class="mb-3">
                        <label class="form-label">Gender</label>
                        <select name="gender" id="gender" class="form-control">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Age</label>
                        <input type="number" name="age" id="age" class="form-control" />
                        <span id="age_error" class="text-danger"></span>
                    </div>
                </div>
-->
                <div class="modal-footer">
                    <input type="hidden" name="id" id="id" />
                    <input type="hidden" name="action" id="action" value="Add" />
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="action_button">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>

$(document).ready(function(){

    load_data();

    function load_data()
    {
        var seconds = new Date() / 1000;

        $.getJSON("data.json?"+seconds+"", function(data){

            data.sort(function(a,b){

                return b.id - a.id;

            });

            var data_arr = [];

            for(var count = 0; count < data.length; count++)
            {
                var sub_array = {
                    'productname' : data[count].productname,
                    'productid' : data[count].productid,
                    'productqtt' : data[count].productqtt,
                    'gender' : data[count].gender,
                    'producttype' : data[count].producttype,
                    'action' : '<button type="button" class="btn btn-success btn-sm edit rounded-0" data-id="'+data[count].id+'"><i class="fa fa-edit"></i> Edit</button>&nbsp;<button type="button" class="btn btn-danger btn-sm delete rounded-0" data-id="'+data[count].id+'"><i class="fa fa-trash"></i> Delete</button>'
                };

                data_arr.push(sub_array);
            }

            $('#sample_data').DataTable({
                data : data_arr,
                order : [],
                columns : [
                    { data : "productname" },
                    { data : "productid" },
                    { data : "productqtt" },
                    { data : "gender" },
                    { data : "producttype" },
                    { data : "action" }
                ]
            });

        });
    }

    $('#add_data').click(function(){

        $('#dynamic_modal_title').text('Ajouter un Produit');

        $('#sample_form')[0].reset();

        $('#action').val('Add');

        $('#action_button').text('Ajouter');

        $('.text-danger').text('');

        $('#action_modal').modal('show');

    });

    $('#sample_form').on('submit', function(event){

        event.preventDefault();

        $.ajax({
            url:"action.php",
            method:"POST",
            data:$('#sample_form').serialize(),
            dataType:"JSON",
            beforeSend:function()
            {
                $('#action_button').attr('disabled', 'disabled');
            },
            success:function(data)
            {
                $('#action_button').attr('disabled', false);
                if(data.error)
                {
                    if(data.error.productname_error)
                    {
                        $('#productname_error').text(data.error.productname_error);
                    }
                    if(data.error.productid_error)
                    {
                        $('#productid_error').text(data.error.productid_error);
                    }
                    if(data.error.producttype_error)
                    {
                        $('#producttype_error').text(data.error.producttype_error);
                    }
                }
                else
                {
                    $('#message').html('<div class="alert alert-success">'+data.success+'</div>');

                    $('#action_modal').modal('hide');

                    $('#sample_data').DataTable().destroy();

                    load_data();

                    setTimeout(function(){
                        $('#message').html('');
                    }, 5000);
                }
            }
        });

    });

    $(document).on('click', '.edit', function(){
        console.log($('#select_qtt').val());
        var id = $(this).data('id');

        $('#dynamic_modal_title').text('Edit Data');

        $('#action').val('Edit');

        $('#action_button').text('Editer');

        $('.text-danger').text('');

        $('#action_modal').modal('show');

        $.ajax({
            url:"action.php",
            method:"POST",
            data:{id:id, action:'fetch_single'},
            dataType:"JSON",
            success:function(data)
            {
                $('#productname').val(data.productname);
                $('#productid').val(data.productid);
                $('#productqtt').val(data.productqtt);
                $('#gender').val(data.gender);
                $('#producttype').val(data.producttype);
                $('#id').val(data.id);
            }
        });

    });

    $(document).on('click', '.delete', function(){

        var id = $(this).data('id');

        if(confirm("Etes vous sûr de vouloir supprimer ce produit ?"))
        {
            $.ajax({
                url:"action.php",
                method:"POST",
                data:{action:'delete', id:id},
                dataType:"JSON",
                success:function(data)
                {
                    $('#message').html('<div class="alert alert-success">'+data.success+'</div>');
                    $('#sample_data').DataTable().destroy();
                    load_data();
                    setTimeout(function(){
                        $('#message').html('');
                    }, 5000);
                }
            });
        }

    });

});

</script>