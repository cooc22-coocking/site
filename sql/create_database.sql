DROP DATABASE IF EXISTS coocking;

CREATE DATABASE coocking;

CREATE TABLE coocking.produits (
	id_produit INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nom_produit VARCHAR (50) NOT NULL
);

CREATE TABLE coocking.etiquette (
	id_etiquette INT NOT NULL PRIMARY KEY,
	quantite INT,
	id_produit INT REFERENCES produits(id_produit)
);
