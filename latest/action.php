<?php

//action.php

if(isset($_POST["action"]))
{
	$file = 'data.json';

	if($_POST['action'] == 'Add' ||$_POST['action'] == 'Edit')
	{
		$error = array();

		$data = array();

		$data['id'] = time();

		if(empty($_POST['productname']))
		{
			$error['productname_error'] = 'Donnez un nom au Produit';
		}
		else
		{
			$data['productname'] = trim($_POST['productname']);
		}

		
		if(empty($_POST['productid']))
		{
			$error['productid_error'] = 'Donnez un ID au Produit';
		}
		else
		{
			$data['productid'] = trim($_POST['productid']);
		}

		$data['productqtt'] = trim($_POST['productqtt']);
		$data['gender'] = trim($_POST['gender']);

		if(empty($_POST['producttype']))
		{
			$error['producttype_error'] = 'Indiquez la quantité';
		}
		else
		{
			$data['producttype'] = trim($_POST['producttype']);
		}

		if(count($error) > 0)
		{
			$output = array(
				'error'		=>	$error
			);
		}
		else
		{
			$file_data = json_decode(file_get_contents($file), true);

			if($_POST['action'] == 'Add')
			{

				$file_data[] = $data;

				file_put_contents($file, json_encode($file_data));

				$output = array(
					'success' => 'Le produit a bien été ajouté'
				);
			}

			if($_POST['action'] == 'Edit')
			{
				$key = array_search($_POST['id'], array_column($file_data, 'id'));

				$file_data[$key]['productname'] = $data['productname'];

				$file_data[$key]['productid'] = $data['productid'];

				$file_data[$key]['producttype'] = $data['producttype'];

				$file_data[$key]['gender'] = $data['gender'];

				$file_data[$key]['productqtt'] = $data['productqtt'];

				file_put_contents($file, json_encode($file_data));

				$output = array(
					'success' => 'Le produit a bien été édité'
				);
			}
		}

		echo json_encode($output);
	}

	if($_POST['action'] == 'fetch_single')
	{
		$file_data = json_decode(file_get_contents($file), true);

		$key = array_search($_POST["id"], array_column($file_data, 'id'));

		echo json_encode($file_data[$key]);
	}

	if($_POST['action'] == 'delete')
	{
		$file_data = json_decode(file_get_contents($file), true);

		$key = array_search($_POST['id'], array_column($file_data, 'id'));

		unset($file_data[$key]);

		file_put_contents($file, json_encode($file_data));

		echo json_encode(['success' => 'Le produit a bien été supprimé']);

	}
}

?>